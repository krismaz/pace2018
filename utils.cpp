#include "utils.h"

SteinerTree mapEdges(const SteinerTree &tree, map <int,vector<Edge>> &edgeIdMapping) {
    SteinerTree mapped;
    for (auto &e: tree) {
        if (edgeIdMapping.count(e.id)) {
            copy(edgeIdMapping[e.id].begin(), edgeIdMapping[e.id].end(), back_inserter(mapped));
        } else {
            mapped.push_back(e);
        }
    }

    return mapped;
}

vector <vector<pair<int,edge_t>>> getAdjacencyLists(const Graph &graph) {
    vector <vector<pair<int,edge_t>>> adj(graph.nodes);
    for (auto &e : graph.edges) {
        adj[e.u].push_back({e.v, e.length});
        adj[e.v].push_back({e.u, e.length});
    }

    return adj;
}

vector <vector<Edge>> getAdjacentEdges(const Graph &graph) {
    vector <vector<Edge>> adj(graph.nodes);
    for (auto &e : graph.edges) {
        adj[e.u].push_back(e);
        adj[e.v].push_back(e);
    }

    return adj;
}

template <typename ans_t>
vector <ans_t> dijkstra(int source, int nodes, vector <vector<pair<int,edge_t>>> &adj, ans_t INF) {
    vector <ans_t> dist(nodes, INF);
    dist[source] = 0;

    priority_queue <pair<ans_t,int>> q;
    q.push({0, source});

    while (!q.empty()) {
        auto e = q.top(); q.pop();
        int v = e.second;

        if (e.first != -dist[v]) continue;

        for (auto &e : adj[v]) {
            int u = e.first;

            if (dist[u] > dist[v] + e.second) {
                dist[u] = dist[v] + e.second;
                q.push({-dist[u], u});
            }
        }
    }

    return dist;
}

/* Dijkstra algorithm that does not visit vertices farther than 2 * the longest edge from source */
template <typename ans_t>
vector <ans_t> dijkstraNeighbors(int source, int nodes, vector <vector<pair<int,edge_t>>> &adj, ans_t INF) {
    vector <ans_t> dist(nodes, INF);

    edge_t breakAfter = 0;
    for (auto &e : adj[source]) {
        breakAfter = max(breakAfter, 2 * e.second);
    }

    dist[source] = 0;

    priority_queue <pair<ans_t,int>> q;
    q.push({0, source});

    while (!q.empty()) {
        auto e = q.top(); q.pop();
        int v = e.second;

        if (e.first != -dist[v]) continue;
        if (dist[v] > breakAfter) break;

        for (auto &e : adj[v]) {
            int u = e.first;

            if (dist[u] > dist[v] + e.second) {
                dist[u] = dist[v] + e.second;
                q.push({-dist[u], u});
            }
        }
    }

    return dist;
}

/* Shortest path from source to target that does not pass through a given edge */
template <typename ans_t>
ans_t shortestAvoidingPath(int source, int target, int fromAvoid, int toAvoid, vector <vector<pair<int,int64_t>>> &adj, ans_t INF) {
    vector <ans_t> dist(adj.size(), INF);
    dist[source] = 0;

    priority_queue <pair<ans_t,int>> q;
    q.push({0, source});

    while (!q.empty()) {
        auto e = q.top(); q.pop();
        int v = e.second;

        if (e.second == target) return dist[target];
        if (e.first != -dist[v]) continue;

        for (auto &e : adj[v]) {
            int u = e.first;
            if (v == fromAvoid && u == toAvoid) {
                continue;
            }

            if (dist[u] > dist[v] + e.second) {
                dist[u] = dist[v] + e.second;
                q.push({-dist[u], u});
            }
        }
    }

    return INF;
}

template <typename ans_t>
vector <vector<ans_t>> allShortestPaths(const Graph &graph, ans_t INF) {
    auto adj = getAdjacencyLists(graph);

    vector <vector<ans_t>> dist;
    for (int i = 0; i < graph.nodes; i++) {
        dist.push_back(dijkstra <ans_t> (i, graph.nodes, adj, INF));
    }

    return dist;
}

template <typename ans_t>
vector <vector<ans_t>> allShortestPathsNeighbors(const Graph &graph, ans_t INF) {
    auto adj = getAdjacencyLists(graph);

    vector <vector<ans_t>> dist;
    for (int i = 0; i < graph.nodes; i++) {
        dist.push_back(dijkstraNeighbors <ans_t> (i, graph.nodes, adj, INF));
    }

    return dist;
}

template <typename ans_t>
vector <vector<ans_t>> termsShortestPaths(const Graph &graph, ans_t INF) {
    auto adj = getAdjacencyLists(graph);

    vector <vector<ans_t>> dist;
    for (int t : graph.terms) {
        dist.push_back(dijkstra <ans_t> (t, graph.nodes, adj, INF));
    }

    return dist;
}

template <typename ans_t>
vector <vector<int>> allFirstOnPath(const Graph &graph, vector <vector<ans_t>> &dist, ans_t INF) {
    auto adj = getAdjacencyLists(graph);

    auto firstOnPath = vector <vector<int>> (graph.nodes, vector <int> (graph.nodes, -1));
    for (int u = 0; u < graph.nodes; u++) {
        for (int v = 0; v < graph.nodes; v++) {
            for (auto &e : adj[u]) {
                int first; edge_t len;
                tie(first, len) = e;

                if (dist[u][v] == len + dist[first][v]) {
                    firstOnPath[u][v] = first;
                }
            }
        }
    }

    return firstOnPath;
}

Edge getEdge(int u, int v, const Graph &graph) {
    return *find_if(graph.edges.begin(), graph.edges.end(), [&](const Edge &e) {
        return minmax(e.u, e.v) == minmax(u, v);
    });
}

vector <Edge> getEdgesOnPath(int u, int v, const Graph &graph, vector <vector<int>> &firstOnPath) {
    vector <Edge> result;
    while (u != v) {
        int next = firstOnPath[u][v];
        result.push_back(getEdge(u, next, graph));

        u = next;
    }

    return result;
}

/* I think we should never actually need to reverse this operation */
void contractEdge(int u, int v, Graph &graph) {
    if (u > v) swap(u, v);

    graph.edges.erase(remove_if(graph.edges.begin(), graph.edges.end(), [&](const Edge &e) {
        return min(e.u, e.v) == u && max(e.u, e.v) == v;
    }), graph.edges.end());

    auto newId = [&](int x) {
        return x == v ? u : x - (x > v);
    };

    for (auto &e : graph.edges) {
        e.u = newId(e.u);
        e.v = newId(e.v);
    }

    auto uPos = find(graph.terms.begin(), graph.terms.end(), u);
    auto vPos = find(graph.terms.begin(), graph.terms.end(), v);

    if (uPos != graph.terms.end() && vPos != graph.terms.end()) {
        graph.terms.erase(vPos);
    }

    for (int &t : graph.terms) {
        t = newId(t);
    }

    graph.nodes--;
}

void removeVertices(const set <int> &verts, Graph &graph) {
    auto removed = [&](int v) { return verts.count(v); };

    graph.edges.erase(remove_if(graph.edges.begin(), graph.edges.end(), [&](const Edge &e) {
        return removed(e.u) || removed(e.v);
    }), graph.edges.end());

    graph.terms.erase(remove_if(graph.terms.begin(), graph.terms.end(), [&](int u) {
        return removed(u);
    }), graph.terms.end());

    vector <int> idsLeft;
    for (int i = 0; i < graph.nodes; i++) {
        if (!removed(i)) {
            idsLeft.push_back(i);
        }
    }

    auto newId = [&](int x) { return lower_bound(idsLeft.begin(), idsLeft.end(), x) - idsLeft.begin(); };

    for (auto &e : graph.edges) {
        e.u = newId(e.u);
        e.v = newId(e.v);
    }

    for (int &t : graph.terms) {
        t = newId(t);
    }

    graph.nodes = idsLeft.size();
}

void removeVertex(int v, Graph &graph) { removeVertices({v}, graph); }

FindAndUnion::FindAndUnion(int n): p(n,-1) {}

int FindAndUnion::find(int x) {
    if (p[x] == -1) return x;
    return p[x] = find(p[x]);
}

void FindAndUnion::join(int x, int y) {
    x = find(x); y = find(y);
    if (x != y) p[x] = y;
}

string info(const Graph &graph) {
    ostringstream buf;
    buf << "nodes: " << graph.nodes
        << ", edges: " << graph.edges.size()
        << ", terminals: " << graph.terms.size();
    return buf.str();
}

bool isConnected(const Graph &graph) {
    FindAndUnion fau(graph.nodes);
    for (auto &e : graph.edges) {
        fau.join(e.u, e.v);
    }

    for (int i = 1; i < graph.nodes; i++) {
        if (fau.find(i) != fau.find(0)) {
            return false;
        }
    }

    return true;
}

void removeEmptyComponents(Graph &graph) {
    FindAndUnion fau(graph.nodes);
    for (auto &e : graph.edges) {
        fau.join(e.u, e.v);
    }
    int component = fau.find(graph.terms.front());
    for (int t : graph.terms)
        assert(component == fau.find(t));
    set<int> toRemove;
    for (int i = 0;  i < graph.nodes; ++i)
        if (fau.find(i) != component)
            toRemove.insert(i);
    removeVertices(toRemove, graph);
}


vector <int> getArticulationPoints(const Graph &graph, const vector <int> &block) {
    int t = 0;

    vector <bool> isBlocked(graph.nodes, false);
    for (int v : block) {
        isBlocked[v] = true;
    }

    vector <int> d(graph.nodes, -1), low(graph.nodes, -1);
    vector <vector<int>> adj(graph.nodes), revInd(graph.nodes);

    for (auto &e : graph.edges) {
        if (isBlocked[e.u] || isBlocked[e.v]) {
            continue;
        }

        revInd[e.u].push_back(adj[e.v].size());
        revInd[e.v].push_back(adj[e.u].size());

        adj[e.u].push_back(e.v);
        adj[e.v].push_back(e.u);
    }

    vector <int> art;

    function <void(int, int)> dfs = [&](int v, int parInd) {
        int s = 0, par = (parInd == -1)? -1 : adj[v][parInd];
        bool isArt = false;

        d[v] = low[v] = t++;

        for (int i = 0; i < (int) adj[v].size(); i++) {
            int w = adj[v][i];

            if (d[w] == -1) {
                if (par == -1) s++;
                dfs(w, revInd[v][i]);

                low[v] = min(low[v], low[w]);

                if (low[w] >= d[v]) {
                    isArt = true;
                }
            } else if (i != parInd) {
                low[v] = min(low[v], d[w]);
            }
        }

        if (par == -1) {
            isArt = (s > 1);
        }

        if (isArt) {
            art.push_back(v);
        }
    };

    for (int i = 0; i < graph.nodes; i++) {
        if (!isBlocked[i] && d[i] == -1) {
            dfs(i, -1);
        }
    }

    return art;
}

Timer::Timer() : last(Clock::now()) {}

int Timer::done(const char* label) {
    Clock::time_point now = Clock::now();
    int result = chrono::duration_cast<chrono::milliseconds>(now - last).count();
    last = now;
    if (label)
        LOG << label << " " << result << "ms";
    return result;
}

bool isValidAnswer(const Graph &graph, const SteinerTree &tree) {
    FindAndUnion fau(graph.nodes);
    for (auto &e : tree) {
        if (min(e.u, e.v) < 0 || max(e.u, e.v) >= graph.nodes) return false;
        if (fau.find(e.u) == fau.find(e.v)) return false;

        fau.join(e.u, e.v);
    }

    int root = fau.find(graph.terms[0]);
    for (int i = 1; i < (int) graph.terms.size(); i++) {
        if (fau.find(graph.terms[i]) != root) {
            return false;
        }
    }

    return true;
}

template vector <int32_t> dijkstra <int32_t> (int source, int nodes, vector <vector<pair<int,edge_t>>> &adj, int32_t INF);
template vector <int64_t> dijkstra <int64_t> (int source, int nodes, vector <vector<pair<int,edge_t>>> &adj, int64_t INF);

template int32_t shortestAvoidingPath <int32_t> (int source, int target, int fromAvoid, int toAvoid, vector <vector<pair<int,int64_t>>> &adj, int32_t INF);
template int64_t shortestAvoidingPath <int64_t> (int source, int target, int fromAvoid, int toAvoid, vector <vector<pair<int,int64_t>>> &adj, int64_t INF);

template vector <vector<int32_t>> allShortestPaths <int32_t> (const Graph &graph, int32_t INF);
template vector <vector<int64_t>> allShortestPaths <int64_t> (const Graph &graph, int64_t INF);

template vector <vector<int32_t>> allShortestPathsNeighbors <int32_t> (const Graph &graph, int32_t INF);
template vector <vector<int64_t>> allShortestPathsNeighbors <int64_t> (const Graph &graph, int64_t INF);

template vector <vector<int32_t>> termsShortestPaths <int32_t> (const Graph &graph, int32_t INF);
template vector <vector<int64_t>> termsShortestPaths <int64_t> (const Graph &graph, int64_t INF);

template vector <vector<int>> allFirstOnPath <int32_t> (const Graph &graph, vector <vector<int32_t>> &dist, int32_t INF);
template vector <vector<int>> allFirstOnPath <int64_t> (const Graph &graph, vector <vector<int64_t>> &dist, int64_t INF);

