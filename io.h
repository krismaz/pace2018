#ifndef PACE2018_IO_H
#define PACE2018_IO_H

#include "common.h"

Graph readGraph();
void printAnswer(const Graph &graph, const SteinerTree &tree);

#endif //PACE2018_IO_H
