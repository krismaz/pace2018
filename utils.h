#ifndef PACE2018_UTILS_H
#define PACE2018_UTILS_H

#include "common.h"

template <typename ans_t>
ans_t getCost(const SteinerTree &tree);

SteinerTree mapEdges(const SteinerTree &tree, map <int,vector<Edge>> &edgeIdMapping);

vector <vector<pair<int,edge_t>>> getAdjacencyLists(const Graph &graph);
vector <vector<Edge>> getAdjacentEdges(const Graph &graph);

template <typename ans_t>
vector <ans_t> dijkstra(int source, int nodes, vector <vector<pair<int,edge_t>>> &adj, ans_t INF);

template <typename ans_t>
ans_t shortestAvoidingPath(int source, int target, int fromAvoid, int toAvoid, vector <vector<pair<int,edge_t>>> &adj, ans_t INF);

template <typename ans_t>
vector <vector<ans_t>> allShortestPaths(const Graph &graph, ans_t INF);

template <typename ans_t>
vector <vector<ans_t>> allShortestPathsNeighbors(const Graph &graph, ans_t INF);

template <typename ans_t>
vector <vector<ans_t>> termsShortestPaths(const Graph &graph, ans_t INF);

template <typename ans_t>
vector <vector<int>> allFirstOnPath(const Graph &graph, vector <vector<ans_t>> &dist, ans_t INF);

Edge getEdge(int u, int v, const Graph &graph);
vector <Edge> getEdgesOnPath(int u, int v, const Graph &graph, vector <vector<int>> &firstOnPath);
void contractEdge(int u, int v, Graph &graph);
void removeVertices(const set <int> &verts, Graph &graph);
void removeVertex(int v, Graph &graph);

struct FindAndUnion {
    vector <int> p;
    FindAndUnion(int n);

    int find(int x);
    void join(int x, int y);
};

string info(const Graph& graph);

bool isConnected(const Graph &graph);

void removeEmptyComponents(Graph &graph);

vector <int> getArticulationPoints(const Graph &graph, const vector <int> &block);

struct Timer {
    typedef chrono::high_resolution_clock Clock;
    Clock::time_point last;
    Timer();

    int done(const char* label = nullptr);
};

struct Log {
    ~Log() {
#ifndef NOLOG
        cerr << endl;
#endif
    }
};

template<typename T>
const Log& operator<<(const Log& log, const T& t) {
#ifndef NOLOG
    cerr << t;
#endif
    return log;
}

#define LOG Log()

inline void printGraphInfo(const Graph &graph) { LOG << info(graph); }

bool isValidAnswer(const Graph &graph, const SteinerTree &tree);

#endif //PACE2018_UTILS_H
