#include "utils.h"

#define scanf(T...) assert(scanf(T) != EOF)

Graph readGraph() {
    int nodes, edges;
    scanf("SECTION Graph\n");
    scanf("Nodes %d\n", &nodes);
    scanf("Edges %d\n", &edges);

    Graph graph(nodes);

    while (edges--) {
        int u, v;
        long long length;

        scanf("E %d %d %lld\n", &u, &v, &length); u--; v--;
        graph.addEdge(u, v, length);
    }

    scanf("END\n\n");
    scanf("SECTION Terminals\n");

    int terms;
    scanf("Terminals %d\n", &terms);

    while (terms--) {
        int t;
        scanf("T %d\n", &t); t--;

        graph.addTerm(t);
    }

    scanf("END\n\n");
    scanf("EOF\n");

    return graph;
}

void sanityCheck(const Graph &graph, const SteinerTree &tree) {
    FindAndUnion fau(graph.nodes);
    for (auto &e : tree) {
        assert(fau.find(e.originalU) != fau.find(e.originalV));
        fau.join(e.originalU, e.originalV);
    }

    int root = fau.find(graph.terms[0]);
    for (int i = 1; i < (int) graph.terms.size(); i++) {
        assert(fau.find(graph.terms[i]) == root);
    }
}

void printAnswer(const Graph &graph, const SteinerTree &tree) {
    sanityCheck(graph, tree);

    printf("VALUE %lld\n", (long long) getCost <int64_t> (tree));

    for (auto &e : tree) {
        printf("%d %d\n", 1 + e.originalU, 1 + e.originalV);
    }
}
