#!/usr/bin/env python3

import argparse
import glob
import os
import resource
import subprocess
import sys


DIR = 'tests'

def get_number_of_states(name):
    with open(name) as inp:
        while True:
            line = inp.readline().strip()
            if line.startswith("Nodes"):
                nodes = int(line.split()[-1])
            elif line.startswith("Terminals"):
                terms = int(line.split()[-1])
            elif line == "EOF":
                break
    
    return nodes * (2 ** (terms - 1))

def run(binary, timeout):
    instances = sorted(glob.glob(os.path.join(DIR, '*.gr')))
    print('Testing %s on %d instances' % (binary, len(instances)))
    
    for instance in instances:
        timed_out = False
        total_states = get_number_of_states(instance)
        num = instance[-6:-3]
        
        with open(instance) as inp:
            try:
                cputime = resource.getrusage(resource.RUSAGE_CHILDREN).ru_utime
                completed = returncode = subprocess.run(binary,
                    stdin=inp, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                    timeout=timeout)
                err = completed.stderr.decode("utf-8") 
            except subprocess.TimeoutExpired:
                timed_out = True
        if timed_out:
            print(num, 'TLE')
        elif completed.returncode != 0:
            print(num, 'RTE')
        else:
            print(num, 'OK', err, total_states)
        sys.stdout.flush()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('binary')
    parser.add_argument('-t', '--timeout', type=int, default=1800)
    args = parser.parse_args()
    
    print('reduceTime [ms], solveTime [ms], totalTime [ms], visitedStates, totalStates')
    run(args.binary, args.timeout)


if __name__ == '__main__':
    main()
