#ifndef PACE2018_REDUCE_H
#define PACE2018_REDUCE_H

#include "utils.h"

/* it's useful to assume that there are two parts of the cut, even if technically it creates more disjoint components */
struct Cut {
    int score;

    array <vector<int>,2> parts;
    vector <int> cutVertices;
};

void findCut(Graph &graph, int left, vector <int> vertsTaken, Cut &bestCut);

template <typename ans_t>
void reduce(Graph &graph, SteinerTree &addToAnswer, map <int,vector<Edge>> &edgeIdMapping, ans_t INF, int maxIterations = -1);

#endif //PACE2018_REDUCE_H
