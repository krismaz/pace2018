#include "utils.h"

template <typename mask_t> int popcount(mask_t mask);
template <> int popcount(int32_t mask) { return __builtin_popcount(mask); }
template <> int popcount(int64_t mask) { return __builtin_popcountll(mask); }
template <> int popcount(__int128_t  mask) {
  return __builtin_popcountll(mask >> 64) + __builtin_popcountll(mask);
}

template <typename ans_t>
ans_t getMst(const vector <vector<ans_t>> &dist, const vector <int> &verts, ans_t INF) {
    int t = verts.size();

    ans_t value = 0;

    vector <bool> taken(t, false);
    vector <ans_t> minEdge(t, INF);

    minEdge[0] = 0;

    for (int iter = 0; iter < t; iter++) {
        auto it = min_element(minEdge.begin(), minEdge.end());

        value += *it;
        *it = INF;

        int v = it - minEdge.begin();
        taken[v] = true;

        for (int u = 0; u < t; u++) {
            if (!taken[u]) {
                minEdge[u] = min(minEdge[u], dist[verts[u]][verts[v]]);
            }
        }
    }

    return value;
}

template <typename ans_t>
ans_t getMSTUpperBound(const Graph &graph, ans_t INF) {
    return getMst <ans_t> (allShortestPaths <ans_t> (graph, INF), graph.terms, INF);
}

template <typename ans_t>
ans_t getPathHeuristicUpperBound(const Graph &graph, ans_t INF) {
    vector<vector<ans_t>> distanceFromTerminal(graph.terms.size());
    auto adj = getAdjacencyLists(graph);
    for (int i = 0; i < (int)graph.terms.size(); ++i) {
        distanceFromTerminal[i] = dijkstra<ans_t>(graph.terms[i], graph.nodes, adj, INF);
    }
    ans_t upperBound = INF;
    for (int start = 0; start < (int)graph.terms.size(); ++start) {
        ans_t value = 0;
        vector<ans_t> distToTree(graph.terms.size());
        for (int i = 0; i < (int)graph.terms.size(); ++i)
            distToTree[i] = distanceFromTerminal[start][graph.terms[i]];
        vector<bool> inTree(graph.nodes, false);
        inTree[graph.terms[start]] = true;
        for (int round = 0; round < (int)graph.terms.size() - 1; ++round) {
            int t = -1;
            for (int i = 0; i < (int)graph.terms.size(); ++i)
                if (distToTree[i] > 0 && (t == -1 || distToTree[i] < distToTree[t]))
                    t = i;
            value += distToTree[t];
            assert(t != -1);
            int s = -1;
            for (int i = 0; i < graph.nodes; ++i)
                if (inTree[i] && (s == -1 || distanceFromTerminal[t][i] < distanceFromTerminal[t][s]))
                    s = i;
            assert(s != -1);
            vector<int> path;
            do {
                int next = -1;
                for (const auto& edge : adj[s]) {
                    if (distanceFromTerminal[t][edge.first] + edge.second == distanceFromTerminal[t][s]) {
                        next = edge.first;
                        break;
                    }
                }
                assert(next != -1);
                s = next;
                path.push_back(s);
            } while (s != graph.terms[t]);
            for (int v : path) {
                inTree[v] = true;
                for (int i = 0; i < (int)graph.terms.size(); ++i)
                    distToTree[i] = min(distToTree[i], distanceFromTerminal[i][v]);
            }
        }
        upperBound = min(upperBound, value);
    }
    return upperBound;
}

template <typename ans_t>
SteinerTree inclusionExclusionAlgorithm(Graph graph, ans_t upperBound) {
    int s0 = graph.terms[0];
    const long long T = 1LL << graph.terms.size();

    auto adj = getAdjacencyLists(graph);

    vector <long long> count(upperBound + 1, 0);
    vector <vector<long long>> countWithEdge(upperBound + 1, vector <long long> (graph.nextEdgeId, 0));

    for (long long mask = 0; mask < T; mask++) {
        vector <bool> inMask(graph.nodes, false);
        for (int i = 0; i < graph.terms.size(); i++) {
            if (mask & (1LL << i)) {
                inMask[graph.terms[i]] = true;
            }
        }

        vector <vector<long long>> dp(graph.nodes, vector <long long> (upperBound + 1, 0));
        for (int w = 0; w <= upperBound; w++) {
            for (int x = 0; x < graph.nodes; x++) {
                if (!inMask[x]) {
                    if (w == 0) {
                        dp[x][w] = 1;
                        continue;
                    }

                    for (auto &e : adj[x]) {
                        if (inMask[e.first]) {
                            continue;
                        }

                        ans_t rest = w - e.second;
                        for (ans_t w1 = 0; w1 <= rest; w1++) {
                            dp[x][w] += dp[e.first][w1] * dp[x][rest-w1];
                        }
                    }
                }
            }

            int sign = popcount(mask) % 2 ? -1 : 1;
            count[w] += dp[s0][w] * sign;

            for (auto &e : graph.edges) {
                ans_t rest = w - e.length;
                for (ans_t w1 = 0; w1 <= rest; w1++) {
                    countWithEdge[w][e.id] += dp[e.u][w1] * dp[e.v][rest-w1] * sign;
                }
            }
        }
    }

    ans_t minCost = find_if(count.begin(), count.end(), [](long long x) { return x != 0; }) - count.begin();

    SteinerTree ans;
    ans_t cost = 0;

    for (auto &e : graph.edges) {
        if (countWithEdge[minCost][e.id] != 0) {
            ans.push_back(e);
            cost += e.length;
        }
    }

    if (cost > minCost) {
        auto e = ans[0];

        graph.edges = ans;
        contractEdge(e.u, e.v, graph);

        ans = inclusionExclusionAlgorithm(graph, minCost);
        ans.push_back(e);
    }

    return ans;
}

template <typename ans_t>
SteinerTree dwAlgorithm(const Graph &graph, ans_t INF) {
    auto dist = allShortestPaths <ans_t> (graph, INF);
    auto firstOnPath = allFirstOnPath <ans_t> (graph, dist, INF);

    const int T = 1 << graph.terms.size();
    vector <vector<ans_t>> dp(T, vector <ans_t> (graph.nodes, INF));

    for (int i = 0; i < (int) graph.terms.size(); i++) {
        dp[1 << i][graph.terms[i]] = 0;
    }

    for (int mask = 1; mask < T; mask++) {
        // Loop unrolling
        #define F { ans_t h = dp[sub][v] + dp[mask ^ sub][v]; dp[mask][v] = h < dp[mask][v] ? h : dp[mask][v]; ++v; }

        for (int sub = (mask - 1) & mask; sub; sub = (sub - 1) & mask) {
            int v = 0;
            while (v + 4 <= graph.nodes) {
                F F F F
            }

            while (v < graph.nodes) {
                F
            }
        }

        #define F2 { ans_t h = dp[mask][u] + dist[v][u]; dp[mask][v] = h < dp[mask][v] ? h : dp[mask][v]; ++u; }

        for (int v = 0; v < graph.nodes; v++) {
            int u = 0;
            while (u + 4 <= graph.nodes) {
                F2 F2 F2 F2
            }

            while (u < graph.nodes) {
                F2
            }
        }
    }

    int bestRoot = min_element(dp[T-1].begin(), dp[T-1].end()) - dp[T-1].begin();

    SteinerTree solution;
    function <void(int,int)> constructSolution = [&](int mask, int v) -> void {
        for (int u = 0; u < graph.nodes; u++) if (u != v) {
            if (dp[mask][v] == dp[mask][u] + dist[u][v]) {
                constructSolution(mask, u);

                auto edgesOnPath = getEdgesOnPath(v, u, graph, firstOnPath);
                copy(edgesOnPath.begin(), edgesOnPath.end(), back_inserter(solution));

                return ;
            }
        }

        for (int submask = (mask - 1) & mask; submask; submask = (submask - 1) & mask) {
            if (dp[mask][v] == dp[submask][v] + dp[mask ^ submask][v]) {
                constructSolution(submask, v);
                constructSolution(mask ^ submask, v);

                return ;
            }
        }
    };

    constructSolution(T-1, bestRoot);
    return solution;
}

template <typename T, typename S>
struct VectMap {
    vector <S> vec;

    VectMap(T size, S init): vec(size, init) {}

    S get(T x) { return vec[x]; }

    void set(T x, S val) { vec[x] = val; }
};

template <typename T, typename S>
struct HashMap {
    vector <pair<T,S>> map;
    pair <T,S> init;

    int mod = 5001, fill = 0;
    HashMap(T size, S initValue) {
        init.second = initValue;
        map.resize(mod, init);
    }

    int getPos(T x) {
        int i = (x ^ (x >> 16)) % mod;

        while (map[i].first != x && map[i] != init) {
            i++;
            if (i == mod) {
                i = 0;
            }
        }

        return i;
    }

    S get(T x) {
        return map[getPos(x)].second;
    }

    void set(T x, S val) {
        int pos = getPos(x);
        fill += (map[pos] == init);

        map[pos] = {x, val};

        if (2 * fill > mod) {
            auto prevMap = map;

            mod = 2 * mod + 1;
            map = vector <pair<T,S>> (mod, init);

            fill = 0;

            for (auto &e : prevMap) {
                if (e != init) {
                    set(e.first, e.second);
                }
            }
        }
    }
};

template <typename ans_t>
int getCentralTerm(const Graph &graph, const vector <vector<ans_t>> &dist, ans_t INF) {
    int centralTerm = -1;
    ans_t totalDist = INF;

    for (int i = 0; i < (int) graph.terms.size(); i++) {
        ans_t sum = 0;
        for (int j = 0; j < (int) graph.terms.size(); j++) {
            sum += dist[graph.terms[i]][graph.terms[j]];
        }

        if (sum < totalDist) {
            totalDist = sum;
            centralTerm = i;
        }
    }

    return centralTerm;
}

// TODO: an alternative implementation when upperbound is large, with ans_t as key
template <typename T>
struct Queue {
    int curr = 0;
    vector <vector<T>> q;

    Queue(int bound): q(bound + 1) {}

    void put(int key, T val) { q[key].push_back(val); }

    T get() {
        while (curr < (int) q.size() && q[curr].empty()) curr++;
        assert(curr < (int) q.size());

        T val = q[curr].back();
        q[curr].pop_back();

        return val;
    }
};

template <typename ans_t, typename mask_t, template <typename, typename> typename map_t, typename bitmap_t>
SteinerTree dijkstraSteinerAlgorithmImpl(Graph graph, ans_t INF, Stats &stats) {
    auto dist = allShortestPaths <ans_t> (graph, INF);
    auto adj = getAdjacencyLists(graph);

    int rootTermIndex = getCentralTerm(graph, dist, INF);
    swap(graph.terms[rootTermIndex], graph.terms.back());

    const mask_t ONE = 1;
    const int terms = (int) graph.terms.size() - 1;
    const mask_t T = ONE << terms;
    const int r0 = graph.terms.back();


    map_t <mask_t,ans_t> mstForMask(ONE << (terms + 1), -1);

    auto getLowerBound = [&](int v, mask_t mask) -> ans_t {
        if (mstForMask.get(mask) == -1) {
            vector <int> verts;
            for (int i = 0; i <= terms; i++) {
                if (mask & (ONE << i)) {
                    verts.push_back(graph.terms[i]);
                }
            }

            mstForMask.set(mask, getMst <ans_t> (dist, verts, INF));
        }

        ans_t bound = mstForMask.get(mask);
        ans_t firstMin = INF, secondMin = INF;
        for (int i = 0; i <= terms; i++) if (mask & (ONE << i)) {
            ans_t d = dist[v][graph.terms[i]];
            if (d < firstMin) {
                secondMin = firstMin;
                firstMin = d;
            } else if (d < secondMin) {
                secondMin = d;
            }
        }

        bound += firstMin;
        bound += secondMin < INF ? secondMin : firstMin;

        return bound / 2;
    };

    vector <map_t <mask_t, ans_t>> l(graph.nodes, map_t <mask_t, ans_t> (T, INF));
    vector <bitmap_t> vis(graph.nodes, bitmap_t (T, false));

    ans_t mstUpperBound = getMst <ans_t> (dist, graph.terms, INF);
    ans_t upperBound = getPathHeuristicUpperBound <ans_t> (graph, INF);
    LOG << "Upper bounds " << mstUpperBound << " " << upperBound;

    Queue <pair<int,mask_t>> nQueue(upperBound + 1);
    vector <vector<mask_t>> visList(graph.nodes);

    map_t <mask_t,ans_t> ub(T, INF);
    map_t <mask_t,mask_t> ubSet(T, 0);
    map_t <mask_t,pair<ans_t,int>> distToRest(T, {-1, -1});

    auto getDistToRest = [&](mask_t mask) -> pair <ans_t,int> {
        if (distToRest.get(mask).first == -1) {
            distToRest.set(mask, {INF, -1});

            for (int i = 0; i < terms; i++) {
                if (mask & (ONE << i)) {
                    for (int j = 0; j <= terms; j++) {
                        if (!(mask & (ONE << j))) {
                            ans_t here = dist[graph.terms[i]][graph.terms[j]];

                            if (here < distToRest.get(mask).first) {
                                distToRest.set(mask, {here, j});
                            }
                        }
                    }
                }
            }
        }

        return distToRest.get(mask);
    };

    auto addToQueue = [&](int v, mask_t mask, ans_t value) {
        ans_t priority = value + getLowerBound(v, (T - 1) ^ mask ^ (ONE << terms));

        if (priority <= upperBound) {
            l[v].set(mask, value);
            nQueue.put(priority, {v, mask});
        }
    };

    for (int i = 0; i < terms; i++) {
        int t = graph.terms[i];
        mask_t mask = ONE << i;

        addToQueue(t, mask, 0);
    }

    for (int i = 0; i < graph.nodes; i++) {
        l[i].set(0, 0);
        vis[i].set(0, true);
    }

    auto relaxSum = [&](int v, mask_t mask1, mask_t mask2, ans_t value1) {
        mask_t sum = mask1 | mask2;
        ans_t newValue = value1 + l[v].get(mask2);

        ans_t myUb;
        if (newValue < l[v].get(sum) && newValue <= (myUb = ub.get(sum))) {
            if (!(ubSet.get(mask1) & mask2) || !(ubSet.get(mask2) & mask1)) {
                ans_t here = ub.get(mask1) + ub.get(mask2);
                if (here < myUb) {
                    mask_t hereSet = ubSet.get(mask1) | ubSet.get(mask2);
                    hereSet &= ~sum;

                    ub.set(sum, myUb = here);
                    ubSet.set(sum, hereSet);

                    if (sum == T - 1) {
                        upperBound = min(upperBound, myUb);
                    }
                }
            }

            if (newValue <= myUb) {
                addToQueue(v, sum, newValue);
            }
        }
    };

    while (!vis[r0].get(T-1)) {
        int v; mask_t mask;
        tie(v, mask) = nQueue.get();

        ans_t myValue;
        if (vis[v].get(mask) || (myValue = l[v].get(mask)) > ub.get(mask)) {
            continue;
        }

        vis[v].set(mask, true);
        visList[v].push_back(mask);

        stats.visitedStates++;

        pair <ans_t,int> dst = getDistToRest(mask);
        for (int i = 0; i <= terms; i++) {
            if (!(mask & (ONE << i))) {
                ans_t here = dist[v][graph.terms[i]];

                if (here < dst.first) {
                    dst = {here, i};
                }
            }
        }

        dst.first += myValue;

        if (dst.first < ub.get(mask)) {
            ub.set(mask, dst.first);
            ubSet.set(mask, ONE << dst.second);

            if (mask == T - 1) {
                upperBound = min(upperBound, dst.first);
            }
        }

        for (auto &e : adj[v]) {
            int u; edge_t len;
            tie(u, len) = e;

            ans_t newValue = myValue + len;
            if (!vis[u].get(mask) && newValue < l[u].get(mask) && newValue <= ub.get(mask)) {
                addToQueue(u, mask, newValue);
            }
        }

        mask_t neg = (T - 1) & (~mask);
        mask_t submasks = ONE << popcount(neg);

        if (submasks < (int) visList[v].size()) {
            for (mask_t sub = neg; sub; sub = (sub - 1) & neg) {
                if (vis[v].get(sub) && !vis[v].get(mask | sub)) {
                    relaxSum(v, mask, sub, myValue);
                }
            }
        } else {
            for (mask_t sub : visList[v]) {
                if (!(sub & mask) && !vis[v].get(mask | sub)) {
                    relaxSum(v, mask, sub, myValue);
                }
            }
        }
    }

    function <SteinerTree(int,mask_t)> backtrack = [&](int v, mask_t mask) -> SteinerTree {
        ans_t myValue;
        if ((myValue = l[v].get(mask)) == 0) {
            return {};
        }

        for (auto &e : adj[v]) {
            int u; edge_t len;
            tie(u, len) = e;

            if (l[u].get(mask) + len == myValue) {
                auto res = backtrack(u, mask);
                res.push_back(getEdge(v, u, graph));

                return res;
            }
        }

        mask_t submasks = ONE << popcount(mask);

        mask_t optSub;
        bool optSubFound = false;
        if (submasks < (int) visList[v].size()) {
            for (mask_t sub = (mask - 1) & mask; sub; sub = (sub - 1) & mask) {
                if (l[v].get(sub) + l[v].get(mask ^ sub) == myValue) {
                    optSub = sub;
                    optSubFound = true;
                    break;
                }
            }
        } else {
            for (mask_t sub : visList[v]) {
                if (sub != mask && (sub & mask) == sub && l[v].get(sub) + l[v].get(mask ^ sub) == myValue) {
                    optSub = sub;
                    optSubFound = true;
                    break;
                }
            }
        }

        assert(optSubFound);

        auto res = backtrack(v, optSub);
        auto add = backtrack(v, mask ^ optSub);

        copy(add.begin(), add.end(), back_inserter(res));
        return res;
    };

    return backtrack(r0, T - 1);
}

template <typename ans_t>
SteinerTree dijkstraSteinerAlgorithm(const Graph &graph, ans_t INF, Stats &stats) {
    int numTerms = graph.terms.size();

    if (numTerms <= 30) {
        const int64_t VALUES_MEM_THRESHOLD = 1LL << 31;
        const int DATA_SIZE = sizeof(ans_t);

        const int64_t ARRAY_ELEMENTS = graph.nodes * (1LL << (numTerms - 1));
        const bool valuesInMap = ARRAY_ELEMENTS * DATA_SIZE > VALUES_MEM_THRESHOLD;

        LOG << "Keep values in map: " << valuesInMap;

        if (valuesInMap) {
            const int64_t VIS_MEM_THRESHOLD = 1LL << 29;
            const bool visInMap = ARRAY_ELEMENTS / 8 > VIS_MEM_THRESHOLD;

            LOG << "Keep visited flag in map: " << visInMap;

            if (visInMap) {
                return dijkstraSteinerAlgorithmImpl <ans_t, int32_t, HashMap, HashMap <int32_t, bool>> (graph, INF, stats);
            } else {
                return dijkstraSteinerAlgorithmImpl <ans_t, int32_t, HashMap, VectMap <int32_t, bool>> (graph, INF, stats);
            }
        } else {
            return dijkstraSteinerAlgorithmImpl <ans_t, int32_t, VectMap, VectMap <int32_t, bool>> (graph, INF, stats);
        }
    } else if (numTerms <= 62) {
        return dijkstraSteinerAlgorithmImpl <ans_t, int64_t, HashMap, HashMap <int64_t, bool>> (graph, INF, stats);
    } else if (numTerms <= 126) {
        return dijkstraSteinerAlgorithmImpl <ans_t, __int128_t, HashMap, HashMap <__int128_t, bool>> (graph, INF, stats);
    } else {
        assert(false);
    }
}

template SteinerTree dwAlgorithm <int32_t> (const Graph &graph, int32_t INF);
template SteinerTree dwAlgorithm <int64_t> (const Graph &graph, int64_t INF);

template SteinerTree dijkstraSteinerAlgorithm <int32_t> (const Graph &graph, int32_t INF, Stats &stats);
template SteinerTree dijkstraSteinerAlgorithm <int64_t> (const Graph &graph, int64_t INF, Stats &stats);

template int32_t getMSTUpperBound <int32_t> (const Graph &graph, int32_t INF);
template int64_t getMSTUpperBound <int64_t> (const Graph &graph, int64_t INF);

template int32_t getPathHeuristicUpperBound <int32_t> (const Graph &graph, int32_t INF);
template int64_t getPathHeuristicUpperBound <int64_t> (const Graph &graph, int64_t INF);
