#!/usr/bin/env python3

import argparse
import glob
import os
import os.path
import re
import resource
import subprocess
import sys
import urllib.request


DIR = 'steinlib'


def stp2gr(fname):
    root, ext = os.path.splitext(fname)
    assert ext == '.stp'
    with open(fname) as inp, open(root + '.gr', 'w') as out:
        for section in ('Graph', 'Terminals'):
            HEAD, Head = 'SECTION %s' % section, 'Section %s' % section
            while True:
                line = inp.readline().strip()
                if line in (HEAD, Head):
                    out.write(HEAD + '\n')
                    break
            TAIL, Tail = 'END', 'End'
            while True:
                line = inp.readline().strip()
                if line in (TAIL, Tail):
                    out.write(TAIL + '\n')
                    break
                else:
                    out.write(line + '\n')
            out.write('\n') # New line at end of section
        out.write('EOF\n')


def download():
    try:
        os.mkdir(DIR)
    except OSError:
        print('Directory %s already exists, remove it before re-downloading datasets' % DIR)
        sys.exit(1)
    # Fetch list of datasets
    with urllib.request.urlopen('http://steinlib.zib.de/download.php') as f:
        html = f.read()
    html = html.decode()
    datasets = re.findall('(?<=download/)\w*(?=.tgz)', html)
    # Remove GENE because it is for (directed) Steiner arborescense problems
    datasets.remove('GENE')
    # Remove 1R and 2R because they have broken edge counts
    datasets.remove('1R')
    datasets.remove('2R')
    # Download each dataset
    for dataset in datasets:
        print('Dataset', dataset)
        # Fetch archive, unzip, and remove
        print('Downloading input')
        archive = '%s.tgz' % dataset
        urllib.request.urlretrieve(
            'http://steinlib.zib.de/download/%s' % archive,
            os.path.join(DIR, archive))
        subprocess.run(['tar' , '-xf', os.path.join(DIR, archive), '--directory', DIR])
        os.remove(os.path.join(DIR, archive))
        # Convert .stp to .gr
        print('Converting to *.gr')
        for fname in glob.glob(os.path.join(DIR, dataset, '*.stp')):
            stp2gr(fname)
        # Fetch correct answers
        print('Downloading output') 
        with urllib.request.urlopen('http://steinlib.zib.de/showset.php?%s' % dataset) as f:
            html = f.read()
        rows = [str(line) for line in html.splitlines() if b'td' in line]
        for row in rows:
            instance = re.search('(?<=&nbsp;)[\w,-]+(?=</td>)', row)
            if instance is None:
                print('Ignoring row, could not find instance name in %s' % row)
                continue
            instance = instance.group()
            result = re.search('(?<=<[b,i]>)\d+(?=</[b,i]>)', row)
            if result is None:
                print('Ignoring row, could not find result in %s' % row)
                continue
            result = result.group()
            with open(os.path.join(DIR, dataset, instance) + '.hint', 'w') as f:
                f.write(result)
    # Dirty hack because some datasets have inconsistent labeling
    for fname in glob.glob(os.path.join(DIR, 'P4Z', 'p*.gr')):
        os.rename(fname, fname.replace('p', 'P'))
    os.rename(os.path.join(DIR, 'P6Z', 'P608.hint'), os.path.join(DIR, 'P6Z', 'p608.hint'))


def check_answer(root):
    with open(root + '.hint') as f:
        correct_value = int(f.read())
    with open (root + '.out') as f:
        line = f.readline()
    return correct_value == int(line[len('VALUE:'):])
    # TODO: check if it is a correct Steiner tree


def run(binary, instances, timeout):
    if not instances:
        instances = glob.glob(os.path.join(DIR, '*', '*.gr'))
    print('Testing %s on %d instances' % (binary, len(instances)))
    TLE = []
    for instance in instances:
        root, ext = os.path.splitext(instance)
        assert ext == '.gr'
        with open(instance) as inp, open(root + '.out', 'w') as out, open(root + '.err', 'w') as err:
            try:
                cputime = resource.getrusage(resource.RUSAGE_CHILDREN).ru_utime
                returncode = subprocess.run(binary,
                    stdin=inp, stdout=out, stderr=err,
                    timeout=timeout).returncode
            except subprocess.TimeoutExpired:
                cputime = timeout
            else:
                cputime = resource.getrusage(resource.RUSAGE_CHILDREN).ru_utime - cputime
        if cputime >= timeout:
            TLE.append(instance)
            print('TLE', instance)
        elif returncode != 0:
            print('RTE', instance)
        else:
            status = 'OK' if check_answer(root) else 'WA'
            print(status, '%.2fs' % cputime, instance)
    print('Done')
    if TLE:
        print('Instances that timed out (you may want to re-run them with higher time limit)')
        print(' '.join(TLE))


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')
    subparsers.add_parser('download')
    parser_run = subparsers.add_parser('run')
    parser_run.add_argument('binary')
    parser_run.add_argument('instances', nargs='*')
    parser_run.add_argument('-t', '--timeout', type=int, default=5)
    args = parser.parse_args()
    if args.command == 'download':
        download()
    else:
        run(args.binary, args.instances, args.timeout)


if __name__ == '__main__':
    main()
