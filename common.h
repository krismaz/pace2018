#ifndef PACE2018_COMMON_H
#define PACE2018_COMMON_H

#include <bits/stdc++.h>

using namespace std;

typedef int64_t edge_t;

struct Edge {
    int u, v; edge_t length, originalLength; int id, originalU, originalV;
};

/* This struct takes linear memory so it can be reused for the heuristics track where often n > 10^4 */
struct Graph {
    int nodes, nextEdgeId = 0;
    vector <int> terms;
    vector <Edge> edges;

    Graph(int nodes): nodes(nodes) {}

    int addEdge(int u, int v, edge_t length) {
        edges.push_back({u, v, length, length, nextEdgeId++, u, v});
        return nextEdgeId - 1;
    }

    void addEdge(const Edge &e) {
        edges.push_back(e);
        nextEdgeId = max(nextEdgeId, e.id + 1);
    }

    void addTerm(int t) { terms.push_back(t); }

    bool isTerm(int t) { return find(terms.begin(), terms.end(), t) != terms.end(); }
};

typedef vector <Edge> SteinerTree;

template <typename ans_t>
ans_t getCost(const SteinerTree &tree) {
    ans_t cost = 0;
    for (auto &e : tree) {
        cost += e.originalLength;
    }

    return cost;
}

template int32_t getCost <int32_t> (const SteinerTree &tree);
template int64_t getCost <int64_t> (const SteinerTree &tree);

struct Stats {
    int32_t reduceTime = 0, solveTime = 0, totalTime = 0;
    int64_t visitedStates = 0;
};

#endif //PACE2018_COMMON_H
