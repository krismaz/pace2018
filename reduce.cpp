#include "reduce.h"
#include "algo.h"

vector <int> getDegrees(Graph &graph) {
    vector <int> degree(graph.nodes, 0);
    for (auto& e : graph.edges) {
        degree[e.u]++;
        degree[e.v]++;
    }

    return degree;
}

void addToMapping(int from, const vector <Edge> &to, map <int,vector<Edge>> &edgeIdMapping) {
    for (auto &e : to) {
        if (edgeIdMapping.count(e.id)) {
            copy(edgeIdMapping[e.id].begin(), edgeIdMapping[e.id].end(), back_inserter(edgeIdMapping[from]));
        } else {
            edgeIdMapping[from].push_back(e);
        }
    }
}

template <typename ans_t>
bool reduceDegreeTwo(Graph &graph, SteinerTree &addToAnswer, map <int,vector<Edge>> &edgeIdMapping, ans_t INF) {
    auto degree = getDegrees(graph);
    for (int u = 0; u < graph.nodes; u++) {
        if (degree[u] == 2 && find(graph.terms.begin(), graph.terms.end(), u) == graph.terms.end()) {
            vector <int> neighbors;
            vector <Edge> neighborsEdges;
            ans_t totalLength = 0;

            for (auto &e : graph.edges) {
                if (e.u == u || e.v == u) {
                    neighbors.push_back(e.u + e.v - u);
                    neighborsEdges.push_back(e);
                    totalLength += e.length;
                }
            }

            auto adj = getAdjacencyLists(graph);
            ans_t dist = shortestAvoidingPath <ans_t> (neighbors[0], neighbors[1], neighbors[0], u, adj, INF);

            if (dist > totalLength) {
                graph.edges.erase(remove_if(graph.edges.begin(), graph.edges.end(), [&](const Edge& e) -> bool {
                    return minmax(e.u, e.v) == minmax(neighbors[0], neighbors[1]);
                }), graph.edges.end());

                int newEdgeId = graph.addEdge(neighbors[0], neighbors[1], totalLength);
                addToMapping(newEdgeId, neighborsEdges, edgeIdMapping);
            }

            removeVertex(u, graph);
            return true;
        }
    }

    return false;
}

// TODO: remove all nodes that currently are leaves at once (edgecase when there are only two vertices left)
bool reduceLeaves(Graph &graph, SteinerTree &addToAnswer) {
    if (graph.terms.size() <= 1) return false;

    auto degree = getDegrees(graph);
    auto it = find(degree.begin(), degree.end(), 1);

    if (it == degree.end()) {
        return false;
    } else {
        int u = it - degree.begin();
        Edge e = *find_if(graph.edges.begin(), graph.edges.end(), [&](const Edge& e) { return e.u == u || e.v == u; });

        if (find(graph.terms.begin(), graph.terms.end(), u) != graph.terms.end()) {
            addToAnswer.push_back(e);
        }

        contractEdge(e.u, e.v, graph);
        return true;
    }
}

template <typename ans_t>
bool reduceWithSpecialDistance(Graph &graph, map <int,vector<Edge>> &edgeIdMapping, ans_t INF) {
    auto dist = allShortestPathsNeighbors <ans_t> (graph, INF);
    auto distTerm = termsShortestPaths <ans_t> (graph, INF);

    auto getSpecialDist = [&](int u, int v) {
        ans_t specialDistance = dist[u][v];
        for (int i = 0; i < (int) graph.terms.size(); i++) {
            specialDistance = min(specialDistance, max(distTerm[i][u], distTerm[i][v]));
        }

        for (int i = 0; i < (int) graph.terms.size(); i++) {
            for (int j = 0; j < (int) graph.terms.size(); j++) {
                specialDistance = min(
                    specialDistance,
                    max({distTerm[i][u], distTerm[i][graph.terms[j]], distTerm[j][v]})
                );
            }
        }

        return specialDistance;
    };

    bool changed = false;
    for (int i = 0; i < (int) graph.edges.size(); ) {
        int u = graph.edges[i].u, v = graph.edges[i].v;
        ans_t specialDistance = getSpecialDist(u, v);

        if (specialDistance < graph.edges[i].length) {
            graph.edges.erase(graph.edges.begin() + i);
            changed = true;
        } else {
            i++;
        }
    }

    auto adj = getAdjacencyLists(graph);
    auto adjEdges = getAdjacentEdges(graph);
    auto degree = getDegrees(graph);

    vector <Edge> removedEdges;

    auto affected = [&](int u, int v) {
        for (auto &e : removedEdges) {
            if (dist[u][e.u] + e.length + dist[v][e.v] == dist[u][v]) {
                return true;
            }
        }

        return false;
    };

    auto getSpecialDistEq = [&](int u, int v, int edgeId) {
        ans_t specialDistance = INF;
        vector <int> opt;

        for (auto &first : adjEdges[u]) {
            if (first.id == edgeId) continue;

            ans_t len = first.length;
            int x = first.u + first.v - u;

            ans_t here = len + dist[v][x];
            if (here < specialDistance) {
                specialDistance = here;
                opt = {u, v};
            }
        }

        vector <int> otherTerms;
        for (int i = 0; i < (int) graph.terms.size(); i++) {
            if (graph.terms[i] != u && graph.terms[i] != v) {
                otherTerms.push_back(i);
            }
        }

        for (int i : otherTerms) {
            ans_t here = max(distTerm[i][u], distTerm[i][v]);

            if (here < specialDistance) {
                specialDistance = here;
                opt = {u, graph.terms[i], v};
            }
        }

        for (int i : otherTerms) {
            for (int j : otherTerms) {
                ans_t here = max({distTerm[i][u], distTerm[i][graph.terms[j]], distTerm[j][v]});

                if (here < specialDistance) {
                    specialDistance = here;
                    opt = {u, graph.terms[i], graph.terms[j], v};
                }
            }
        }

        for (int i = 0; i < (int) opt.size() - 1; i++) {
            if (affected(opt[i], opt[i+1])) {
                return INF;
            }
        }

        return specialDistance;
    };

    for (int i = 0; i < (int) graph.edges.size();) {
        int u = graph.edges[i].u, v = graph.edges[i].v;
        ans_t specialDistance = getSpecialDistEq(u, v, graph.edges[i].id);

        if (specialDistance <= graph.edges[i].length) {
            auto e = graph.edges[i];

            removedEdges.push_back(e);

            swap(e.u, e.v);
            removedEdges.push_back(e);

            graph.edges.erase(graph.edges.begin() + i);
            changed = true;
        } else {
            i++;
        }
    }

    if (!removedEdges.empty()) {
        return true;
    }

    set <int> toRemove;
    for (int u = 0; u < graph.nodes; u++) {
        if (degree[u] >= 3 && degree[u] <= 5 && find(graph.terms.begin(), graph.terms.end(), u) == graph.terms.end()) {
            bool good = true;

            vector <int> neighbors;
            for (auto& e : adj[u]) {
                neighbors.push_back(e.first);
            }

            for (int mask = 1; mask < (1 << degree[u]); mask++) {
                ans_t sum = 0;
                vector <int> delta;

                for (int i = 0; i < degree[u]; i++) {
                    if (mask & (1 << i)) {
                        delta.push_back(neighbors[i]);
                        sum += dist[u][neighbors[i]];
                    }
                }

                if (delta.size() < 3) continue;

                vector <pair<ans_t, pair<int, int>>> edgesSpecial;
                for (int i = 0; i < (int) delta.size(); i++) {
                    for (int j = i + 1; j < (int) delta.size(); j++) {
                        ans_t specialDistance = min(getSpecialDist(delta[i], delta[j]), getSpecialDist(delta[j], delta[i]));
                        edgesSpecial.push_back({specialDistance, {i, j}});
                    }
                }

                sort(edgesSpecial.begin(), edgesSpecial.end());

                FindAndUnion fau(delta.size());
                ans_t mst = 0;

                for (auto &e : edgesSpecial) {
                    auto p = e.second;
                    if (fau.find(p.first) != fau.find(p.second)) {
                        mst += e.first;
                        fau.join(p.first, p.second);
                    }
                }

                if (mst > sum) {
                    good = false;
                    break;
                }
            }

            if (good) {
                vector <Edge> neighborsEdges = adjEdges[u];
                for (int i = 0; i < degree[u]; i++) {
                    for (int j = i + 1; j < degree[u]; j++) {
                        ans_t len = neighborsEdges[i].length + neighborsEdges[j].length;

                        int newEdgeId = graph.addEdge(neighbors[i], neighbors[j], len);
                        addToMapping(newEdgeId, {neighborsEdges[i], neighborsEdges[j]}, edgeIdMapping);
                    }
                }

                toRemove.insert(u);
                changed = true;

                for (int v : neighbors) {
                    degree[v] = -1; // cannot consider v in this call as it's neighborhood changed
                }
            }
        }
    }

    removeVertices(toRemove, graph);
    return changed;
}

template <typename ans_t>
bool reduceWithTerminalDistance(Graph &graph, SteinerTree &addToAnswer, ans_t INF) {
    auto edges = graph.edges;
    sort(edges.begin(), edges.end(), [&](const Edge &a, const Edge &b) { return a.length < b.length; });

    FindAndUnion fau(graph.nodes);
    vector <Edge> mst;

    for (auto &e : edges) {
        if (fau.find(e.u) != fau.find(e.v)) {
            mst.push_back(e);
            fau.join(e.u, e.v);
        }
    }

    auto dist = termsShortestPaths <ans_t> (graph, INF);
    for (int i = 0; i < (int) mst.size(); i++) {
        FindAndUnion fauSplit(graph.nodes);
        for (int j = 0; j < (int) mst.size(); j++) {
            if (j != i) {
                fauSplit.join(mst[j].u, mst[j].v);
            }
        }

        set <int> seen;
        for (int t : graph.terms) {
            seen.insert(fauSplit.find(t));
        }

        if (seen.size() < 2) continue;

        Edge firstEdge = {-1, -1, INF}, secondEdge = {-1, -1, INF};
        for (auto &e : edges) {
            if (fauSplit.find(e.u) != fauSplit.find(e.v)) {
                if (e.length < firstEdge.length) {
                    secondEdge = firstEdge;
                    firstEdge = e;
                } else if (e.length < secondEdge.length) {
                    secondEdge = e;
                }
            }
        }

        bool take = false;

        if (secondEdge.length == INF) {
            take = true;
        } else {
            ans_t sum = firstEdge.length;

            ans_t uMin = INF, vMin = INF;
            for (int i = 0; i < (int) graph.terms.size(); i++) {
                if (fauSplit.find(graph.terms[i]) == fauSplit.find(firstEdge.u)) {
                    uMin = min(uMin, dist[i][firstEdge.u]);
                } else {
                    vMin = min(vMin, dist[i][firstEdge.v]);
                }
            }

            sum += uMin;
            sum += vMin;

            if (sum <= secondEdge.length) {
                take = true;
            }
        }

        if (take) {
            addToAnswer.push_back(firstEdge);
            contractEdge(firstEdge.u, firstEdge.v, graph);

            return true;
        }
    }

    return false;
}

bool reduceCloseTerms(Graph &graph, SteinerTree &addToAnswer) {
    auto edges = graph.edges;
    sort(edges.begin(), edges.end(), [&](const Edge &a, const Edge &b) { return a.length < b.length; });

    FindAndUnion fau(graph.nodes);

    auto isTerm = [&](int t) -> bool {
        return find(graph.terms.begin(), graph.terms.end(), t) != graph.terms.end();
    };

    for (int i = 0; i < (int) edges.size(); ) {
        int j = i + 1;
        while (j < (int) edges.size() && edges[i].length == edges[j].length) {
            j++;
        }

        for (int k = i; k < j; k++) {
            auto e = edges[k];

            if (fau.find(e.u) != fau.find(e.v)) {
                if (isTerm(e.u) && isTerm(e.v)) {
                    addToAnswer.push_back(e);
                    contractEdge(e.u, e.v, graph);

                    return true;
                }
            }
        }

        for (int k = i; k < j; k++) {
            fau.join(edges[k].u, edges[k].v);
        }

        i = j;
    }

    return false;
}

template <typename ans_t>
bool reduceLongEdges(Graph &graph, ans_t INF) {
    int t = graph.terms.size();
    auto dist = termsShortestPaths <ans_t> (graph, INF);

    ans_t value = 0;

    vector <bool> taken(t, false);
    vector <ans_t> minEdge(t, INF);

    minEdge[0] = 0;

    for (int iter = 0; iter < t; iter++) {
        auto it = min_element(minEdge.begin(), minEdge.end());

        value = max(value, *it);
        *it = INF;

        int v = it - minEdge.begin();
        taken[v] = true;

        for (int u = 0; u < t; u++) {
            if (!taken[u]) {
                minEdge[u] = min(minEdge[u], dist[u][graph.terms[v]]);
            }
        }
    }

    int numEdges = graph.edges.size();

    graph.edges.erase(remove_if(graph.edges.begin(), graph.edges.end(), [&](const Edge &e) {
        return e.length > value;
    }), graph.edges.end());

    return (int) graph.edges.size() < numEdges;
}

// TODO: consider cuts with all terminals in one component
void findCut(Graph &graph, int left, vector <int> vertsTaken, Cut &bestCut) {
    assert(left <= 3);

    if (left == 1) {
        vector <int> art = getArticulationPoints(graph, vertsTaken);

        for (int artPoint : art) {
            vector <int> totalTaken = vertsTaken;
            totalTaken.push_back(artPoint);

            auto taken = [&](int v) { return find(totalTaken.begin(), totalTaken.end(), v) != totalTaken.end(); };

            FindAndUnion fau(graph.nodes);
            for (auto &e : graph.edges) {
                if (!taken(e.u) && !taken(e.v)) {
                    fau.join(e.u, e.v);
                }
            }

            array <vector<int>, 2> parts;
            int firstRoot = -1;

            for (int u = 0; u < graph.nodes; u++) {
                if (!taken(u)) {
                    if (firstRoot == -1) {
                        firstRoot = fau.find(u);
                    }

                    parts[fau.find(u) != firstRoot].push_back(u);
                }
            }

            if (!parts[1].empty()) { // part[0] non-empty by definition
                int minTerms = graph.terms.size();
                for (auto &part : parts) {
                    int terms = 0;
                    for (int u : part) {
                        terms += find(graph.terms.begin(), graph.terms.end(), u) != graph.terms.end();
                    }

                    minTerms = min(minTerms, terms);
                }

                if (minTerms > bestCut.score) {
                    bestCut = {minTerms, parts, totalTaken};
                }
            }
        }
    } else {
        int upperBound = vertsTaken.empty() ? graph.nodes : vertsTaken.back();
        for (int v = 0; v < upperBound; v++) {
            vertsTaken.push_back(v);

            findCut(graph, left - 1, vertsTaken, bestCut);
            vertsTaken.pop_back();
        }
    }
}

template <typename ans_t>
bool reduceWithVoronoi(Graph &graph, ans_t INF) {
    if (graph.terms.size() == 1)  // TODO: does it ever happen ?
        return false;
    vector<vector<ans_t>> distanceFromTerminal(graph.terms.size());
    auto adj = getAdjacencyLists(graph);
    for (int i = 0; i < (int)graph.terms.size(); ++i) {
        distanceFromTerminal[i] = dijkstra<ans_t>(graph.terms[i], graph.nodes, adj, INF);
    }
    ans_t upperBound = getPathHeuristicUpperBound(graph, INF);
    vector<int> base(graph.nodes);  // index of closest terminal
    for (int i = 0; i < graph.nodes; ++i) {
        base[i] = 0;
        for (int j = 1; j < (int)graph.terms.size(); ++j)
            if (distanceFromTerminal[base[i]][i] > distanceFromTerminal[j][i])
                base[i] = j;
    }
    vector<ans_t> radius(graph.terms.size(), INF);  // shortest path leaving voronoi region
    for (int i = 0; i < (int)graph.terms.size(); ++i)
        for (int j = 0; j < graph.nodes; ++j)
            if (base[j] != i)
                radius[i] = min(radius[i], distanceFromTerminal[i][j]);
            sort(radius.begin(), radius.end());
            ans_t radiusSumAllButTwo = 0;
            for (int i = 0; i < (int)radius.size() - 2; ++i)
                radiusSumAllButTwo += radius[i];
    for (int i = 0; i < graph.nodes; ++i) {
        vector<ans_t> distanceToTerminal(graph.terms.size());
        for (int j = 0; j < (int)graph.terms.size(); ++j)
            distanceToTerminal[j] = distanceFromTerminal[j][i];
        sort(distanceToTerminal.begin(), distanceToTerminal.end());
        if (distanceToTerminal[0] == 0)
            continue;
        if (distanceToTerminal[0] + distanceToTerminal[1] + radiusSumAllButTwo > upperBound) {
            removeVertex(i, graph);
            LOG << "Removing vertex with Voronoi, " << info(graph);
            return true;
        }
    }
    for (int i = 0; i < (int)graph.edges.size(); ++i) {
        const Edge& edge = graph.edges[i];
        if (edge.length + distanceFromTerminal[base[edge.u]][edge.u] + distanceFromTerminal[base[edge.v]][edge.v] + radiusSumAllButTwo > upperBound) {
            graph.edges.erase(graph.edges.begin() + i);
            LOG << "Removing edge with Voronoi, " << info(graph);
            return true;
        }
    }
    return false;
}

bool reduceArticulationPoints(Graph &graph) {
    auto art = getArticulationPoints(graph, {});

    for (int v : art) {
        if (!graph.isTerm(v)) {
            FindAndUnion fau(graph.nodes);
            for (auto &e : graph.edges) {
                if (e.u != v && e.v != v) {
                    fau.join(e.u, e.v);
                }
            }

            set <int> hasTerms;
            for (int t : graph.terms) {
                hasTerms.insert(fau.find(t));
            }

            set <int> toRemove;
            for (int i = 0; i < graph.nodes; i++) {
                if (i != v && !hasTerms.count(fau.find(i))) {
                    toRemove.insert(i);
                }
            }

            if (!toRemove.empty()) {
                removeVertices(toRemove, graph);
                return true;
            }
        }
    }

    return false;
}

template <typename ans_t>
void reduceHeavyTerms(Graph &graph, ans_t INF) {
    for (auto &e : graph.edges) {
        if (graph.isTerm(e.u) && graph.isTerm(e.v)) {
            return ;
        }
    }

    auto adj = getAdjacentEdges(graph);
    auto dist = allShortestPaths <ans_t> (graph, INF);

    for (int t : graph.terms) {
        ans_t commonLength = -1;
        bool possible = true;

        for (auto &e : adj[t]) {
            if (commonLength == -1) {
                commonLength = e.length;
            } else if (commonLength != e.length) {
                possible = false;
                break;
            }
        }

        if (!possible) {
            continue;
        }

        ans_t maxDist = 0;
        for (auto &e : adj[t]) {
            for (auto &f : adj[t]) {
                maxDist = max(maxDist, dist[e.u + e.v - t][f.u + f.v - t]);
            }
        }

        if (maxDist >= commonLength) {
            continue;
        }

        ans_t newLength = maxDist + 1;
        LOG << "Lowering all edges adjacent to a terminal from " << commonLength
            << " to " << newLength;

        for (auto &e : graph.edges) {
            if (e.u == t || e.v == t) {
                e.length = newLength;
            }
        }
    }
}

template <typename ans_t>
void reduce(Graph &graph, SteinerTree &addToAnswer, map <int,vector<Edge>> &edgeIdMapping, ans_t INF, int maxIterations) {
    reduceHeavyTerms <ans_t> (graph, INF);

    bool changed;
    do {
        if (maxIterations == 0) break;
        maxIterations--;

        changed = false;

        while (reduceLeaves(graph, addToAnswer)) { changed = true; }
        while (reduceDegreeTwo <ans_t> (graph, addToAnswer, edgeIdMapping, INF)) { changed = true; }

        // Behavior of the || operator ensures that every subsequent reduction can assume graph.terms.size() > 1
        if (graph.terms.size() <= 1) {
            break;
        }

        changed = changed || reduceArticulationPoints(graph);
        // changed = changed || reduceWithVoronoi <ans_t> (graph, INF);
        changed = changed || reduceWithSpecialDistance <ans_t> (graph, edgeIdMapping, INF);
        changed = changed || reduceWithTerminalDistance <ans_t> (graph, addToAnswer, INF);
        changed = changed || reduceCloseTerms(graph, addToAnswer);
        changed = changed || reduceLongEdges <ans_t> (graph, INF);
    } while (changed);

    assert(isConnected(graph));
}

template void reduce <int32_t> (Graph &graph, SteinerTree &addToAnswer, map <int,vector<Edge>> &edgeIdMapping, int32_t INF, int maxIterations);
template void reduce <int64_t> (Graph &graph, SteinerTree &addToAnswer, map <int,vector<Edge>> &edgeIdMapping, int64_t INF, int maxIterations);

