# Exact Steiner Tree with few terminals

This software computes exact Steiner Tree in graphs with few terminal vertices. It was developed for [PACE Challenge 2018](https://pacechallenge.wordpress.com/pace-2018/).

Authors: Krzysztof Maziarz and [Adam Polak](https://adampolak.staff.tcs.uj.edu.pl/)

## Usage

To fetch the code and compile it run:

```
git clone https://adampolak@bitbucket.org/krismaz/pace2018.git
cd pace2018
cmake .
make
```

The program reads its input from `stdin` in the .gr format (a simplified version of the STP format, described on the [PACE website](https://pacechallenge.wordpress.com/pace-2018/))

```
./pace2018 < instanceXYZ.gr
```

You can test the program against SteinLib instances:

```
python3 steiblib.py download
python3 steinlib.py run ./pace2018 -t30
```

You can also remove `-DNOLOG` from `CMakeLists.txt` and recompile to get some diagnostic information printed to `stderr`.

## Algorithm

We use 3^#terminals dynamic programming algorithm by Dreyfus and Wagner. The [Dijkstra-meets-Steiner](https://arxiv.org/abs/1406.0492) heuristics lets us skip subproblems which cannot be part of optimal solution.

Before running the actual algorithm we do a preprocessing. It includes the following reduction rules:

- remove non-terminal leaves, and contract edges to terminal leaves;
- reduce non-terminals of degree two;
- remove edges longer than the longest edge in minimum spanning tree of the distance graph between terminals;
- *special distance with equality* rule;
- *bottleneck degree 3* rule, with generalization to degree 4 and 5;
- *terminal distance* rule;

All these reduction rules are described in existing literature on Steiner tree problem, see e.g. [Uchoa et al. 1999](http://www.inf.puc-rio.br/~poggi/rel-stein.ps).

We add three novel preprocessing steps, which further reduce a few instances:

- branching on vertex cuts of size at most three;
- contracting edges connecting two terminals and belonging to the minimum spanning tree of the whole graph;
- reducing edge weights for *heavy terminals*, i.e. terminals for which we can prove they must be leaves in any optimal Steiner tree.
