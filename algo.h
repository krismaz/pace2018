#ifndef PACE2018_ALGO_H
#define PACE2018_ALGO_H

#include "common.h"

template <typename ans_t>
ans_t getMSTUpperBound(const Graph &graph, ans_t INF);

template <typename ans_t>
ans_t getPathHeuristicUpperBound(const Graph &graph, ans_t INF);

template <typename ans_t>
SteinerTree inclusionExclusionAlgorithm(Graph graph, ans_t upperBound);

template <typename ans_t>
SteinerTree dwAlgorithm(const Graph &graph, ans_t INF);

template <typename ans_t>
SteinerTree dijkstraSteinerAlgorithm(const Graph &graph, ans_t INF, Stats &stats);

#endif //PACE2018_ALGO_H
