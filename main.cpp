#include "io.h"
#include "algo.h"
#include "reduce.h"
#include "utils.h"

const int32_t INT_INF = 1 << 29;
const int64_t LL_INF = 1LL << 59;

template <typename ans_t>
SteinerTree runDijkstraSteiner(const Graph &graph, ans_t INF, Stats &stats) {
    Timer dijkstraSteinerTimer;
    auto answer = dijkstraSteinerAlgorithm <ans_t> (graph, INF, stats);

    int timeTaken = dijkstraSteinerTimer.done();
    stats.solveTime += timeTaken;

    LOG << "Solved, took " << timeTaken << "ms";

    return answer;
}

bool isValid(const Graph &initGraph, map <int,Edge> &edgesInit, const SteinerTree &answer) {
    SteinerTree answerInInit;
    for (auto &e : answer) {
        answerInInit.push_back(edgesInit[e.id]);
    }

    return isValidAnswer(initGraph, answerInInit);
}

template <typename ans_t>
SteinerTree solveImpl(Graph graph, ans_t INF, Stats &stats) {
    auto initGraph = graph;

    map <int,Edge> edgesInit;
    for (auto &e : graph.edges) {
        edgesInit[e.id] = e;
    }

    LOG << "Solving " << info(graph);
    assert(isConnected(graph));

    map <int,vector<Edge>> edgeIdMapping;
    SteinerTree addToAnswer;

    Timer reduceTimer;
    reduce <ans_t> (graph, addToAnswer, edgeIdMapping, INF);


    SteinerTree answer;

    Cut cut = {-1};
    const int MAX_EDGES_THRESHOLD[] = {INT_INF, 3000, 1000};
    const int MIN_TERMS_THRESHOLD[] = {1, 5, 8};

    for (int cutSize = 1; cutSize <= min(3, graph.nodes - 2); cutSize++) {
        if ((int) graph.edges.size() > MAX_EDGES_THRESHOLD[cutSize - 1]) break;

        findCut(graph, cutSize, {}, cut);

        if (cut.score >= MIN_TERMS_THRESHOLD[cutSize - 1]) {
            break ;
        } else {
            cut.score = -1;
        }
    }

    bool tryCuts = cut.score > -1;

    int timeTaken = reduceTimer.done();
    stats.reduceTime += timeTaken;

    LOG << "Reduced " << info(graph) << ", took " << timeTaken << "ms";

    if (tryCuts) {
        ans_t currentBestCost = INF;
        int cutSize = cut.cutVertices.size();

        LOG << "Branching on a cut of size " << cutSize;

        for (int mask = 1; mask < (1 << cutSize); mask++) {
            vector <int> cutTerms; // those (and possibly other) cut vertices will be spanned by the final steiner tree
            for (int i = 0; i < cutSize; i++) {
                if (mask & (1 << i)) {
                    cutTerms.push_back(cut.cutVertices[i]);
                }
            }

            sort(cutTerms.begin(), cutTerms.end());

            bool checkedNone = false, checkedFull = false;

            do {
                // no need to consider both a permutation and its reverse
                if (cutTerms[0] > cutTerms.back()) continue;

                int fullMask = (1 << ((int) cutTerms.size() - 1)) - 1;
                for (int joinInLeft = 0; joinInLeft <= fullMask; joinInLeft++) {
                    if (joinInLeft == 0) {
                        if (!checkedNone) {
                            checkedNone = true;
                        } else {
                            continue;
                        }
                    } else if (joinInLeft == fullMask) {
                        if (!checkedFull) {
                            checkedFull = true;
                        } else {
                            continue;
                        }
                    }

                    SteinerTree answerHere;
                    bool bad = false;

                    for (int partId = 0; partId <= 1; partId++) {
                        auto part = cut.parts[partId];
                        int joinInHere = joinInLeft ^ (partId ? fullMask : 0);

                        auto mergedWith = [&](int u) -> int {
                            auto it = find(cutTerms.begin(), cutTerms.end(), u);
                            if (it != cutTerms.end()) {
                                int i = it - cutTerms.begin();
                                while (i > 0 && (joinInHere & (1 << (i - 1)))) {
                                    i--;
                                }

                                int v = cutTerms[i];
                                return u == v ? -1 : v;
                            }

                            return -1;
                        };

                        for (int v : cut.cutVertices) {
                            if (mergedWith(v) == -1) {
                                part.push_back(v);
                            }
                        }

                        sort(part.begin(), part.end());

                        function <int(int)> getId = [&](int u) -> int {
                            if (mergedWith(u) != -1) {
                                return getId(mergedWith(u));
                            }

                            auto it = lower_bound(part.begin(), part.end(), u);
                            if (it == part.end() || *it != u) {
                                return -1;
                            } else {
                                return it - part.begin();
                            }
                        };

                        Graph subGraph(part.size());
                        for (Edge e : graph.edges) {
                            e.u = getId(e.u);
                            e.v = getId(e.v);

                            if (e.u != -1 && e.v != -1 && e.u != e.v) {
                                subGraph.addEdge(e);
                            }
                        }

                        vector<int> terms = graph.terms;
                        copy(cutTerms.begin(), cutTerms.end(), back_inserter(terms));

                        for (int t : terms) {
                            int id = getId(t);
                            if (id != -1 &&
                                find(subGraph.terms.begin(), subGraph.terms.end(), id) == subGraph.terms.end()) {
                                subGraph.addTerm(id);
                            }
                        }

                        if (!isConnected(subGraph)) { // possible if G[V \ cutVertices] has more than 2 components
                            bad = true;
                            break;
                        }

                        SteinerTree subAnswer = solveImpl <ans_t> (subGraph, INF, stats);
                        copy(subAnswer.begin(), subAnswer.end(), back_inserter(answerHere));
                    }

                    if (!bad) {
                        ans_t costHere = getCost <ans_t> (answerHere);
                        if (costHere < currentBestCost) {
                            answer = answerHere;
                            currentBestCost = costHere;
                        }
                    }
                }
            } while (next_permutation(cutTerms.begin(), cutTerms.end()));
        }

        assert(currentBestCost < INF);
    } else {
        answer = runDijkstraSteiner <ans_t> (graph, INF, stats);
    }

    copy(addToAnswer.begin(), addToAnswer.end(), back_inserter(answer));
    answer = mapEdges(answer, edgeIdMapping);

    if (!isValid(initGraph, edgesInit, answer)) {
        assert(tryCuts);

        answer = runDijkstraSteiner <ans_t> (graph, INF, stats);

        copy(addToAnswer.begin(), addToAnswer.end(), back_inserter(answer));
        answer = mapEdges(answer, edgeIdMapping);

        assert(isValid(initGraph, edgesInit, answer));
    }

    return answer;
}

SteinerTree solve(Graph graph, Stats &stats) {
    int64_t sumLength = 0;
    for (auto &e : graph.edges) {
        sumLength += e.length;
    }

    if (sumLength < INT_INF) {
        return solveImpl <int32_t> (graph, INT_INF, stats);
    } else {
        return solveImpl <int64_t> (graph, LL_INF, stats);
    }
}

void checkWithDw(const Graph &graph, const SteinerTree &tree) {
    auto myCost = getCost <int64_t> (tree);
    auto correctCost = getCost <int64_t> (dwAlgorithm <int64_t> (graph, LL_INF));

    cerr << "Costs: " << myCost << ' ' << correctCost << endl;
    assert(myCost == correctCost);
}

ostream& operator<<(ostream &os, const Stats &stats) {
    os << stats.reduceTime << ' '
       << stats.solveTime << ' '
       << stats.totalTime << ' '
       << stats.visitedStates;

    return os;
}

int main() {
    Timer timer;
    Graph graph = readGraph();
    removeEmptyComponents(graph);

    Stats stats;
    SteinerTree answer = solve(graph, stats);

    // checkWithDw(graph, answer);
    printAnswer(graph, answer);
    stats.totalTime = timer.done("Total time");

    // cerr << stats;

    return 0;
}
